# my first program
print "Hello World!"

# A string with quote marks in it!
print "She said \"hello\""

# A number
print 42

# Math
print 9 + 42
print 9 - 42
print 9 * 42
print 9 / 42

# Floating point numbers (decimal points!)
print 9.0
print 9.0 / 42.0
print 9.0 / 42
print 9 / 42.0

# Negative numbers
print -1
print 2 - 1
print 2 -1  # 2 minus 1
print 2 + -1  # 2 plus -1
print 2 +  - 1  # 2 plus -1

# Comparisons
print 42 < 14
print 42 < 140
print 42 < 42
print 42 <= 42
print 42 >= 42
print 14 == 14
print 14 == 14.0 # compare int and float

print 14 == 14.00000000000000000000000000000000000000000001 # True? why?
print 14 == 14.0000000001 # False

# operator precedence
print ( 1 + 3 + 14 + 7 +3 + 2 + 1) / 3  # divide the whole thine
print  1 + 3 + 14 + 7 +3 + 2 + 1 / 3 # divide just the last number
print  1 + 3 + 14 + 7 +3 + 2 + 1 / 3.0 # floating point division

# assigning variables (NEVER use the "false" as a variable name)
false = 42  # assign 42 to the variable named "false"
print false == 42  # test if "false" is 42

print foo # doesn't exist
foo = 7
print foo
foo = 42.0
print foo
foo = 42 + 7.0
print foo
foo = "cat"
print foo
foo = None  # None is special. it means "nothing"
print foo

# variable names
# must start with a letter
# followed by numbers, letters, or _
# Cannot contain a .
# Cannot be a reserved word: and, del, from, not, while, if, etc...
thiefs8d7f89s7f98sd7f9sfdf789FFF_7 = 4 

# Basic types are immutible
a = 1
b = 2
print a
print b
b = a
b = 7
print a # a didn't change!
print b

x = 7
if x > 4:
  print x
 
if x > 9:
  print x

print x > 4

print x > 9

if x > 9:
  print "cat"
else:
  print "dog"
  