f = open("/Users/david/Desktop/rappers_delight_lyrics.txt", "r")
lyrics = f.read()  # read everything from f, as a string, into a new variable called "lyrics"

words = lyrics.split()

unique_words = {}

for word in words:
  word = word.rstrip(",").rstrip(".").rstrip("!").rstrip("?").strip('"').rstrip("'").lower()
  
  #unique_words[word] = unique_words.get(word, 0) + 1

  if unique_words.has_key(word):
    unique_words[word] = unique_words[word] + 1
  else:
    unique_words[word] = 1

word_counts = []
for word,count in unique_words.items():
  word_counts.append( (count, word) )
  
sorted_words = sorted(word_counts, reverse=True)

for count, word in sorted_words:
  print word, count