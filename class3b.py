f = open("/Users/david/Desktop/rappers_delight_lyrics.txt", "r")
lyrics = f.read()  # read everything from f, as a string, into a new variable called "lyrics"

words = lyrics.split()

unique_words = {}

for word in words:
  word = word.rstrip(",").rstrip(".").rstrip("!").rstrip("?").strip('"').rstrip("'").lower()
  
  unique_words[word] = True

for word in unique_words.keys():
  print word